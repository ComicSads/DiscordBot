module gitlab.com/ComicSads/DiscordBot

go 1.12

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/magefile/mage v1.8.0
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472 // indirect
	golang.org/x/sys v0.0.0-20190830142957-1e83adbbebd0 // indirect
)

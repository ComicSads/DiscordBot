In order for program to work, you need to have a json file called tokens.json set up like this in working directory. Token for testing is used by default, unless -b flag is passed

```json
{
	"Main":"________",
	"Testing":"________"
}
```

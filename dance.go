package main

import (
	"github.com/bwmarrin/discordgo"
	"math/rand"
	"sync"
	"time"
)

func dance(s *discordgo.Session, m *discordgo.MessageCreate) {
	ErrLogf(false, "Dance triggered by %s\n", m.Author)
	switch m.Author.ID {
	// ComicSads' ID
	case owner:
	default:
		s.ChannelMessageSend(m.ChannelID, "This command can only be run by the owner of the bot")
		ErrLog(true, "Dance prevented")
		return
	}
	// Create a struct to be used to reset everything to normal
	type preDance struct {
		userID    string
		channelID string
	}
	// Get guild
	g, err := s.State.Guild(m.GuildID)
	if err != nil {
		s.ChannelMessageSend(m.ChannelID, "No thanks")
		ErrLog(true, "Failed to dance")
		return
	}
	// List of users to move
	var users []string
	// Channels we can move them to
	var channels []string
	// Storing where they are now
	var restore []preDance
	// Find out what voice channels we can move people to
	for _, c := range g.Channels {
		// Channel type 2 is voice
		if c.Type == 2 {
			channels = append(channels, c.ID)
		}
	}
	// Get a list of all the users we can move around
	for _, vs := range g.VoiceStates {
		users = append(users, vs.UserID)
		restore = append(restore, preDance{userID: vs.UserID, channelID: vs.ChannelID})
	}
	// Create a waitgroup
	var wg sync.WaitGroup
	firstMessage, err := s.ChannelMessageSend(m.ChannelID, "Dancing around")
	if err != nil {
		ErrLog(true, "Error sending firstMessage dance:", err)
	}
	// For every person in a voice chat
	for _, u := range users {
		// Create a new ticker
		ticker := time.NewTicker(danceTimeInbetween * time.Second)
		go func() {
			// For variable scoping
			u := u
			// Notify that there's a goroutine
			wg.Add(1)
			// Keep track of how many loops we've gone through
			var i int
			// Run every so often (danceTimeInbetween)
			for _ = range ticker.C {
				// Figure out how many channels there are and chose a random one of then
				n := rand.Intn(len(channels))
				chat := channels[n]
				// Move the user to that voice chat
				s.GuildMemberMove(g.ID, u, chat)
				if i > danceLoops {
					// Once we've looped enough, stop the ticker and mark goroutine as finished
					ticker.Stop()
					wg.Done()
					break
				}
				i++
			}
		}()
		time.Sleep(danceTimeInbetweenPeople * time.Millisecond) // wait inbetween each person for more chaos
	}
	// Wait until all goroutines are finished
	wg.Wait()
	// For some reason, restoring people to where they were can take a long time
	finalMessage, err := s.ChannelMessageSend(m.ChannelID, "Alright, Reseting you now")
	if err != nil {
		ErrLog(true, "Error sending finalMessage dance:", err)
	}
	ErrLogf(false, "Moving users back to normal")
	var i int
	for _, og := range restore {
		i++
		s.GuildMemberMove(g.ID, og.userID, og.channelID)
	}
	ErrLogf(false, "Done moving %d users\n", i)

	// Delete the message we sent and show that we're done
	_, err = s.ChannelMessageSend(m.ChannelID, "Done dancing... for now")
	if err != nil {
		ErrLog(true, "Error sending Done Dancing:", err)
	}
	err = s.ChannelMessageDelete(m.ChannelID, finalMessage.ID)
	if err != nil {
		ErrLog(true, "Error Deleting finalMessage:", err)
	}
	err = s.ChannelMessageDelete(m.ChannelID, firstMessage.ID)
	if err != nil {
		ErrLog(true, "Error Deleting finalMessage:", err)
	}
}

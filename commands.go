package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

type BotCom struct {
	Name       string
	HelpString string
	ErrString  string
	Run        func(BotCom) string
}

var Commands = []BotCom{
	// Help has Run setup in startup
	{Name: "help", HelpString: "shows this page lmao"},
	{Name: "ping", HelpString: "pings the bot", Run: Ping},
	{Name: "dadjoke", HelpString: "bot sends a corny joke", ErrString: "ur mom haha nice one me", Run: DadJoke},
	{Name: "recentvideo", HelpString: "bot sends the most recent video Comic made", ErrString: "https://www.youtube.com/watch?v=dQw4w9WgXcQ", Run: RecentVideo},
	{Name: "goodvid", HelpString: "bot sends a video retrieved from https://comicsads.wtf/goodvideo", ErrString: "Aw fuck shit balls cock im not prepared for this shit\nI spent my whole life wanting to be able to give you, random citizen, a video that you would enjoy, but I have failed my purpose...\n\nSubscribe to PewDiePie", Run: GetRandVideo},
}

// Provides information on each command the bot is capable of
func Help(b BotCom) string {
	var send strings.Builder
	send.WriteString(fmt.Sprintf("All commands are case-insensitive, prefixed with a '%s', and usually have a meme when they fail\n", botPrefix))
	for _, c := range Commands {
		send.WriteString(fmt.Sprintf("`%s: %s`\n​", c.Name, c.HelpString))
	}
	return send.String()
}

func Ping(b BotCom) string {
	ErrLogf(false, "Ping triggered by %s\n", b.ErrString)
	return "Pong!"
}

// Gets the most recent youtube video uploaded by ComicSads
func RecentVideo(b BotCom) string {
	fail := func(err error) string {
		ErrLog(true, "Error getting most recent video:", err)
		return b.ErrString
	}
	req, err := http.NewRequest("GET", "http://comicsads.wtf/vid", nil)
	if err != nil {
		return fail(err)
	}
	req.Header.Set("Accept", "text/plain")
	resp, err := netClient.Do(req)
	if err != nil {
		return fail(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fail(err)
	}
	return string(body)
}

// Returns a random video from comicsads.wtf/goodVid
func GetRandVideo(b BotCom) string {
	req, err := http.NewRequest("GET", "http://comicsads.wtf/goodvid", nil)
	if err != nil {
		ErrLog(true, "Error creating http request for random video:", err)
		return b.ErrString
	}
	req.Header.Set("Accept", "text/plain")
	resp, err := netClient.Do(req)
	if err != nil {
		ErrLog(true, "Error getting response for random video:", err)
		return b.ErrString
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		ErrLog(true, "Error reading response for random video:", err)
		return b.ErrString
	}
	return string(body)
}

// Returns a random dadjoke
func DadJoke(b BotCom) string {
	req, err := http.NewRequest("GET", "https://icanhazdadjoke.com", nil)
	req.Header.Set("Accept", "text/plain")
	req.Header.Set("User-Agent", "Discord bot by comicsads.wtf")
	if err != nil {
		ErrLog(true, "Error generating dad joke web request:", err)
		return b.ErrString
	}
	res, err := netClient.Do(req)
	defer res.Body.Close()
	if err != nil {
		ErrLog(true, "Error pulling dad joke:", err)
		return b.ErrString
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		ErrLog(true, "Error reading dad joke response:", err)
		return b.ErrString
	}
	return string(body)
}

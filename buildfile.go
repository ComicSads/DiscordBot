// +build mage

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/magefile/mage/mg" // mg contains helpful utility functions, like Deps
)

// Aliases for functions
var Aliases = map[string]interface{}{
	"Cleanup": Clean,
	"Lt":      LoveTest,
}

const name = "DiscordBot"

// Builds executable
func Build() error {
	fmt.Println("Building...")
	cmd := exec.Command("go", "build", "-o", name)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

// Build and push needed files to server
func Install() error {
	mg.Deps(Build)
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	binary := filepath.Join(dir, name)

	cmd := exec.Command("scp", binary, "alex@comicsads.wtf:/home/alex/newBot")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err = cmd.Run()
	if err != nil {
		return err
	}

	fmt.Println("Cleaning up")

	err = os.RemoveAll(binary)
	return err
}

func Test() error {
	mg.Deps(Build)
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	binary := filepath.Join(dir, name)
	cmd := exec.Command(binary)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	return cmd.Run()
}

func Clean() error {
	dir, err := os.Getwd()
	if err != nil {
		return err
	}
	binary := filepath.Join(dir, name)
	err = os.RemoveAll(binary)
	return err
}

// Builds executable then deletes it
// Used for finding errors in code
func LoveTest() error {
	// Build executable
	buildErr := Build()

	// Remove executable
	// Note that Clean wont return an error if the executable doesn't exist
	// Meaning that if Build can't create the executable, Clean still wont fail
	cleanErr := Clean()

	if cleanErr != nil {
		return cleanErr
	}
	if buildErr != nil {
		return buildErr
	}
	return nil
}

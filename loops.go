package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

var statusLines = []string{"with tiddies", "with bleach", "BOTW 2", "Flappy Bird", "the game called life and other edgy shit", "Sakura Beach", "Senran Kagura for the plot", "Minecraft girlfriend mod", "the banjo"}

// Changes the "Playing" line on Discord
func StatusLine() {
	prevErr := fmt.Errorf("")
	for {
		err := dg.UpdateStatus(0, statusLines[rand.Intn(len(statusLines))])
		if err != nil && prevErr != nil {
			ErrLog(true, "Status line error:", err)
			return
		}
		prevErr = err
		time.Sleep(30 * time.Second)
	}
}

func CheckSubs() {
	t := time.NewTicker(8 * time.Hour)
	for _ = range t.C {
		req, err := http.NewRequest("GET", "http://comicsads.wtf", nil)
		if err != nil {
			ErrLog(true, "Unable to form request for subcount:", err)
			return
		}
		req.Header.Set("Accept", "text/plain")
		res, err := netClient.Do(req)
		if err != nil {
			ErrLog(true, "Unable to make request for subcount:", err)
			return
		}
		defer res.Body.Close()
		count, err := ioutil.ReadAll(res.Body)
		if err != nil {
			ErrLog(true, "Unable to read response for subcount:", err)
			return
		}
		subCount, _ := strconv.Atoi(string(count))
		if subCount >= goal {
			_, err = dg.ChannelMessageSend("617818371098017810", fmt.Sprintf("@everyone Comic has reached %d subscribers!", goal))
			if err != nil {
				ErrLog(true, "Unable to send subscriber milestone announcement:", err)
				return
			}
			ErrLog(true, "Need to update subscriber goal, congrats on passing %d subscribers man", goal)
			// Return to prevent sending the same message every 5 minutes
			return
		}
	}
}

package main

import (
	"encoding/json"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

// Global variables
const (
	// Prefix for commands
	botPrefix = "!"

	// Channel ID
	channelID = "UCyNlm_1nr1QRqPd7FHwS91w"

	// Next subscriber goal
	goal = 50

	// Dance command timings
	// Seconds inbetween moving the same person
	danceTimeInbetween = 1

	// How many times to move each person
	danceLoops = 5

	// How many milliseconds to wait before moving onto the next person
	danceTimeInbetweenPeople = 300

	// The owner of the bot
	owner = "212406127319580672"
)

// Bot token
var token string

// Specify custom client
var netClient = &http.Client{}

type botTokens struct {
	Main    string
	Testing string
}

var tokens botTokens

// Function for logging errors
// Prints a to log output
// If dm is true, it sends a message to the bot owner
func ErrLog(dm bool, a ...interface{}) {
	ErrLogf(dm, "%v\n", a)
}

// Same as ErrLog except allows for formatting using printf
func ErrLogf(dm bool, format string, a ...interface{}) {
	str := fmt.Sprintf(format, a)
	log.Print(str)
	if dm {
		dg.ChannelMessageSend(own.ID, str)
	}
}

func startup() {
	// Random seed
	rand.Seed(time.Now().UnixNano())
	// Set log output to standard output
	log.SetOutput(os.Stdout)
	// Set random seed
	rand.Seed(time.Now().UnixNano())
	s := os.Args[len(os.Args)-1]
	openJsonFile("tokens.json")
	if s == "-b" {
		// Comic sads bot token
		log.Println("Bot token set to ComicSads Bot, don't include \"-b\" to use Testing Bot")
		token = tokens.Main
	} else {
		// Testing bot token
		log.Println("Bot token set to Testing Bot, include \"-b\" to use ComicSads Bot")
		token = tokens.Testing
	}
	Commands[0].Run = Help
}

// Make it possible to send messages from any function
var own *discordgo.Channel
var dg *discordgo.Session

func main() {
	startup()
	// Log in to bot
	session, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatalln("Error logging into bot:", err)
	}
	dg = session
	// Add function for new messages
	dg.AddHandler(newMessage)
	// Create connection to discord
	err = dg.Open()
	if err != nil {
		log.Fatalln("Error creating connection to Discord:", err)
	}
	// Print that bot has started completely
	fmt.Fprintln(os.Stderr, "Bot is now running. Press CTRL-C to exit.")

	// Set
	own, _ = dg.UserChannelCreate(owner)
	// Loop to check for subscriber milestones
	go CheckSubs()
	// Loop to change playing message
	go StatusLine()
	// Trap Ctrl-C to be able to close cleanly
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
	// Add trailing newline for cleanliness
	fmt.Println()
}

// Run on a new message
func newMessage(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.MentionEveryone {
		go Everyone(s, m)
	}
	// If the bot sent the message, ignore it
	if m.Author.ID == s.State.User.ID {
		return
	}
	// Convert message to lowercase
	message := strings.ToLower(m.Content)
	// Commands that don't need the prefix (jokes)
	if strings.HasPrefix(message, "is this") {
		s.ChannelMessageSend(m.ChannelID, "No this is Patrick")
	}

	// Commands that do need the prefix
	// Function checks if the message has the prefix
	message = botMatch(message)
	if message == "dance" {
		dance(s, m)
		return
	}
	for _, c := range Commands {
		if c.Name == message {
			if c.Name == "ping" {
				c.ErrString = m.Author.String()
				s.ChannelMessageSend(m.ChannelID, c.Run(c))
				c.ErrString = ""
				return
			}
			s.ChannelMessageSend(m.ChannelID, c.Run(c))
			return
		}
	}
}

// Takes a string and returns it if it begins with botPrefix constant. Otherwise returns nil string
func botMatch(s string) string {
	if strings.HasPrefix(s, botPrefix) {
		return strings.TrimPrefix(s, botPrefix)
	}
	return ""
}

// Function runs on an @everyone, and for the next 5 minutes it tracks everyone that leaves the server
func Everyone(s *discordgo.Session, m *discordgo.MessageCreate) {
	g, err := s.State.Guild(m.GuildID)
	if err != nil {
		ErrLog(true, "Failed to check @everyone:", err)
		return
	}
	before := g.Members
	time.Sleep(5 * time.Minute)
	after := g.Members
	var b strings.Builder
	if len(after) < len(before) {
		for _, u := range before {
			if !HasMember(u, after) {
				b.WriteString(fmt.Sprintf("%s couldn't handle an @​everyone\n", u.User.Mention()))
			}
		}
	}
	s.ChannelMessageSend(m.ChannelID, b.String())
}

func openJsonFile(s string) {
	// Open File
	file, err := os.Open(s)
	if err != nil {
		log.Fatalln("Error opening json file:", err)
	}
	defer file.Close()
	// Read file as bytes
	jsonBlob, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalln("Error reading json file:", err)
	}
	err = json.Unmarshal(jsonBlob, &tokens)
	if err != nil {
		log.Fatalln("Error understanding json file:", err)
	}
}

func HasMember(m *discordgo.Member, people []*discordgo.Member) bool {
	list := make([]string, len(people))
	for i, v := range people {
		list[i] = v.User.ID
	}
	return contains(m.User.ID, list)
}

func contains(s string, list []string) bool {
	for _, v := range list {
		if s == v {
			return true
		}
	}
	return false
}
